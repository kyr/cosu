/*
  cosu v1.0.0
  by kyr (http://github.com/kxy)
  MIT License
*/

var express = require("express");
var app = express();
var bp = require("body-parser");
var request = require("request");

app.set('views', __dirname + '/views');

app.use(bp.urlencoded({extended: false}));
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
  res.render('index.jade');
});

app.post('/', function (req, res) {
  request("http://osu.ppy.sh/api/get_user?k=" + process.env.OSUKEY + "&u=" + req.body.name + "&type=string", function (error, request, body) {
    var par = JSON.parse(body);
    res.render('res.jade', {
      title: par[0].username + ' - cosu',
      name: par[0].username,
      level: 'lv' + Math.round(par[0].level),
      rank: '#' + par[0].pp_rank  + ' - ',
      total: 'played ' + par[0].playcount + ' matches and scored ' + par[0].total_score + ' points'
    });
  });
});

app.listen(process.env.PORT || 5000);
